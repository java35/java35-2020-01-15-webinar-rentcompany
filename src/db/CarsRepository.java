package db;

import java.util.List;

import db.entities.CarEntity;

public class CarsRepository implements IRepository<CarEntity, String> {

	// CRUD - create, read, update, delete
	
	@Override
	public CarEntity findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarEntity save(CarEntity entity) {
		if (entity == null)
			return null;
		return rentCompany.saveCar(entity);
	}

	@Override
	public List<CarEntity> findAll() {
		rentCompany.findAllCars();
		return null;
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existsById(String id) {
		// TODO Auto-generated method stub
		return false;
	}

}
