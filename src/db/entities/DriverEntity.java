package db.entities;

import java.time.LocalDate;

public class DriverEntity {
	int licenseId;
	String name;
	LocalDate birthDate;
	String phone;
}
