package db.embedded;

import java.util.HashMap;
import java.util.Map;

import db.entities.CarEntity;

public class RentCompanyEmbedded implements IRentCompanyEmbedded {
	private Map<String, CarEntity> cars = new HashMap<>();
	private Map<Integer, CarEntity> drivers = new HashMap<>();
	
	@Override
	public CarEntity saveCar(CarEntity entity) {
		if (entity == null)
			throw new IllegalArgumentException();
		CarEntity carEntity = new CarEntity(entity);
		cars.put(carEntity.getRegNumber(), carEntity);
		return new CarEntity(cars.get(entity.getRegNumber()));
	}

	public void findAllCars() {
		// TODO Auto-generated method stub
		
	}
}
