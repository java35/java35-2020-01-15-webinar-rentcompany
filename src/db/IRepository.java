package db;

import java.util.List;

import db.embedded.RentCompanyEmbedded;


public interface IRepository<E, I> {
	RentCompanyEmbedded rentCompany = new RentCompanyEmbedded();
	
	E findById(I id);
	E save(E entity);
	List<E> findAll();
	int count();
	void delete(I id);
	boolean existsById(I id);
}